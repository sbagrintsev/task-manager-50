package ru.tsc.bagrintsev.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.SneakyThrows;
import org.bson.Document;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.sevice.ILoggerService;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.LinkedHashMap;
import java.util.Map;

public class LoggerService implements ILoggerService {

    @Nullable
    private MongoClient mongoClient;

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();

    @NotNull
    private final ObjectMapper yamlMapper = new YAMLMapper();


    @SneakyThrows
    public void logToConsole(@NotNull final String json) {
        System.out.println(json);
    }

    @SneakyThrows
    public void logToFile(@NotNull final String json) {
        @NotNull final Map<String, Object> map = objectMapper.readValue(json, LinkedHashMap.class);
        @NotNull final String yaml = yamlMapper.writeValueAsString(map);
        @NotNull final String tableName = map.get("tableName").toString();
        final byte @NotNull [] bytes = (yaml).getBytes();
        @NotNull final File file = new File(tableName);
        file.createNewFile();
        Files.write(Paths.get(tableName), bytes, StandardOpenOption.APPEND);
        System.out.println(file.getAbsolutePath());
    }

    @SneakyThrows
    public void logToMongo(@NotNull final String json) {
        @NotNull final MongoDatabase mongoDatabase = initMongo();
        @NotNull final Map<String, Object> map = objectMapper.readValue(json, LinkedHashMap.class);
        @NotNull final String tableName = map.get("tableName").toString();
        boolean exists = false;
        for (String el : mongoDatabase.listCollectionNames()) {
            if (tableName.equals(el)) {
                exists = true;
                break;
            }
        }
        if (!exists) mongoDatabase.createCollection(tableName);
        @NotNull final MongoCollection<Document> collection = mongoDatabase.getCollection(tableName);
        collection.insertOne(new Document(map));
    }

    private MongoDatabase initMongo() {
        if (mongoClient == null) mongoClient = new MongoClient("localhost", 27017);
        return mongoClient.getDatabase("task-manager");
    }

}
