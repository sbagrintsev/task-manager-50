package ru.tsc.bagrintsev.tm.api.sevice;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.listener.LoggerListener;

public interface IReceiverService {

    void receive(@NotNull LoggerListener listener);

}
