package ru.tsc.bagrintsev.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.model.AbstractWBSModel;

import java.util.List;

public interface IWBSRepository<M extends AbstractWBSModel> extends IUserOwnedRepository<M> {

    @Nullable
    List<M> findAllSort(
            @NotNull final String userId,
            @NotNull final String order
    );

    void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    );

}
