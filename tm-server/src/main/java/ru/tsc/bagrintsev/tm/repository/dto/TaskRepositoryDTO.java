package ru.tsc.bagrintsev.tm.repository.dto;

import jakarta.persistence.EntityManager;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.repository.dto.ITaskRepositoryDTO;
import ru.tsc.bagrintsev.tm.dto.model.TaskDTO;

import java.util.List;

public class TaskRepositoryDTO extends UserOwnedRepositoryDTO<TaskDTO> implements ITaskRepositoryDTO {

    public TaskRepositoryDTO(
            @NotNull final EntityManager entityManager
    ) {
        super(TaskDTO.class, entityManager);
    }

    @Override
    public @Nullable List<TaskDTO> findAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        @NotNull final String jpql = String.format(
                "FROM %s m WHERE m.userId = :userId AND m.projectId = :projectId",
                clazz.getSimpleName());
        return entityManager
                .createQuery(jpql, clazz)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void setProjectId(
            @NotNull final String userId,
            @NotNull final String taskId,
            @Nullable final String projectId
    ) {
        @NotNull final String jpql = String.format("" +
                        "UPDATE %s m " +
                        "SET m.projectId = :projectId " +
                        "WHERE m.userId = :userId " +
                        "AND m.id = :id",
                clazz.getSimpleName());
        entityManager
                .createQuery(jpql)
                .setParameter("projectId", projectId)
                .setParameter("userId", userId)
                .setParameter("id", taskId)
                .executeUpdate();
    }

}
