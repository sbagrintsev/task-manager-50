package ru.tsc.bagrintsev.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.model.ProjectDTO;

import java.util.Collection;
import java.util.List;

public interface IProjectRepositoryDTO extends IUserOwnedRepositoryDTO<ProjectDTO> {

    @Override
    void add(@NotNull final ProjectDTO record);

    @Override
    void addAll(@NotNull final Collection<ProjectDTO> records);

    @Override
    void clear(@NotNull final String userId);

    @Override
    void clearAll();

    @Override
    boolean existsById(
            @NotNull final String userId,
            @NotNull final String id
    );

    @Override
    @Nullable
    List<ProjectDTO> findAll();

    @Override
    @Nullable
    List<ProjectDTO> findAllByUserId(@NotNull final String userId);

    @Override
    @Nullable
    List<ProjectDTO> findAllSort(
            @NotNull final String userId,
            @NotNull final String order
    );

    @Override
    @Nullable
    ProjectDTO findOneById(
            @NotNull final String userId,
            @NotNull final String id
    );

    @Override
    void removeById(
            @NotNull final String userId,
            @NotNull final String id
    );

    @Override
    long totalCount();

    @Override
    long totalCountByUserId(@NotNull final String userId);

    @Override
    void update(@NotNull final ProjectDTO project);

    @Override
    void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    );

}
