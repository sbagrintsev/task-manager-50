package ru.tsc.bagrintsev.tm.repository.model;

import jakarta.persistence.EntityManager;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.repository.model.ISessionRepository;
import ru.tsc.bagrintsev.tm.model.Session;

public class SessionRepository extends UserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository(
            @NotNull final EntityManager entityManager
    ) {
        super(Session.class, entityManager);
    }

}
