package ru.tsc.bagrintsev.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.model.AbstractWBSModelDTO;

import java.util.List;

public interface IUserOwnedRepositoryDTO<M extends AbstractWBSModelDTO> extends IAbstractRepositoryDTO<M> {

    void clear(@NotNull final String userId);

    boolean existsById(
            @NotNull final String userId,
            @NotNull final String id
    );

    @Nullable
    List<M> findAllByUserId(@NotNull final String userId);

    @Nullable
    List<M> findAllSort(
            @NotNull final String userId,
            @NotNull final String order
    );

    @Nullable
    M findOneById(
            @NotNull final String userId,
            @NotNull final String id
    );

    void removeById(
            @NotNull final String userId,
            @NotNull final String id
    );

    long totalCountByUserId(@NotNull final String userId);

    void update(@NotNull final M record);

    void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    );

}
