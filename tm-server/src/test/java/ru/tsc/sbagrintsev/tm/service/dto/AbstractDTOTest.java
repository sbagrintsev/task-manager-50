package ru.tsc.sbagrintsev.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.experimental.categories.Category;
import ru.tsc.bagrintsev.tm.api.sevice.IConnectionService;
import ru.tsc.bagrintsev.tm.api.sevice.IPropertyService;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IProjectServiceDTO;
import ru.tsc.bagrintsev.tm.api.sevice.dto.ITaskServiceDTO;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IUserServiceDTO;
import ru.tsc.bagrintsev.tm.dto.model.UserDTO;
import ru.tsc.bagrintsev.tm.exception.user.LoginAlreadyExistsException;
import ru.tsc.bagrintsev.tm.exception.user.LoginIsIncorrectException;
import ru.tsc.bagrintsev.tm.exception.user.PasswordIsIncorrectException;
import ru.tsc.bagrintsev.tm.service.ConnectionService;
import ru.tsc.bagrintsev.tm.service.PropertyService;
import ru.tsc.bagrintsev.tm.service.dto.ProjectServiceDTO;
import ru.tsc.bagrintsev.tm.service.dto.TaskServiceDTO;
import ru.tsc.bagrintsev.tm.service.dto.UserServiceDTO;
import ru.tsc.sbagrintsev.tm.marker.DBCategory;

import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.List;

@Category(DBCategory.class)
public abstract class AbstractDTOTest {

    @NotNull
    protected static IPropertyService propertyService = new PropertyService();

    @NotNull
    protected static IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    protected ITaskServiceDTO taskService = new TaskServiceDTO(connectionService);

    @NotNull
    protected IProjectServiceDTO projectService = new ProjectServiceDTO(connectionService);

    @NotNull
    protected IUserServiceDTO userService = new UserServiceDTO(projectService, taskService, propertyService, connectionService);

    @AfterClass
    public static void closeConnection() {
        connectionService.close();
    }

    @BeforeClass
    public static void initConnection() {
        connectionService = new ConnectionService(propertyService);
    }

    @After
    public void destroy() {
        taskService.clearAll();
        projectService.clearAll();
        userService.clearAll();
    }

    @Before
    public void init() throws LoginIsIncorrectException, GeneralSecurityException, LoginAlreadyExistsException, PasswordIsIncorrectException {
        projectService = new ProjectServiceDTO(connectionService);
        taskService = new TaskServiceDTO(connectionService);
        userService = new UserServiceDTO(projectService, taskService, propertyService, connectionService);
        @NotNull final UserDTO user1 = userService.create("test1", "testPassword1");
        user1.setId("testUserId1");
        @NotNull final UserDTO user2 = userService.create("test2", "testPassword2");
        user2.setId("testUserId2");
        @NotNull final List<UserDTO> list = Arrays.asList(user1, user2);
        userService.clearAll();
        userService.set(list);
    }

}
