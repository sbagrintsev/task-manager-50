package ru.tsc.bagrintsev.tm.api.sevice;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    IAuthEndpoint getAuthEndpoint();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IDomainEndpoint getDomainEndpoint();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectEndpoint getProjectEndpoint();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ISystemEndpoint getSystemEndpoint();

    @NotNull
    ITaskEndpoint getTaskEndpoint();

    @NotNull
    ITokenService getTokenService();

    @NotNull
    IUserEndpoint getUserEndpoint();

}
